package de.blockbreaker.spawn;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Created by Lukas on 25.05.2015.
 */
public class FileSpawn {

    public static void create() {
        if(!getFile().exists()) {
            FileConfiguration cfg = getFileConfiguration();

            try {
                getFile().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            cfg.addDefault("survival.spawn.world", "world");
            cfg.addDefault("survival.spawn.x", 0);
            cfg.addDefault("survival.spawn.y", 0);
            cfg.addDefault("survival.spawn.z", 0);
            cfg.addDefault("survival.spawn.yaw", 0);
            cfg.addDefault("survival.spawn.pitch", 0);

            cfg.addDefault("builder.spawn.world", "world");
            cfg.addDefault("builder.spawn.x", 0);
            cfg.addDefault("builder.spawn.y", 0);
            cfg.addDefault("builder.spawn.z", 0);
            cfg.addDefault("builder.spawn.yaw", 0);
            cfg.addDefault("builder.spawn.pitch", 0);

            try {
                cfg.save(getFile());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static File getFile() {
        return new File("plugins/SpawnSystem", "config.yml");
    }

    public static FileConfiguration getFileConfiguration() {
        return YamlConfiguration.loadConfiguration(getFile());
    }

    public static void setSurvivalSpawn(String world, double x, double y, double z, float yaw, float pitch) {
        getFileConfiguration().set("survival.spawn.world", world);
        getFileConfiguration().set("survival.spawn.x", x);
        getFileConfiguration().set("survival.spawn.y", y);
        getFileConfiguration().set("survival.spawn.z", z);
        getFileConfiguration().set("survival.spawn.yaw", yaw);
        getFileConfiguration().set("survival.spawn.pitch", pitch);

        try {
            getFileConfiguration().save(getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void setBuilderSpawn(String world, double x, double y, double z, float yaw, float pitch) {
        getFileConfiguration().set("builder.spawn.world", world);
        getFileConfiguration().set("builder.spawn.x", x);
        getFileConfiguration().set("builder.spawn.y", y);
        getFileConfiguration().set("builder.spawn.z", z);
        getFileConfiguration().set("builder.spawn.yaw", yaw);
        getFileConfiguration().set("builder.spawn.pitch", pitch);

        try {
            getFileConfiguration().save(getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Location getBuilderSpawn() {
        String world = getFileConfiguration().getString("builder.spawn.world");
        double x = getFileConfiguration().getDouble("builder.spawn.x");
        double y = getFileConfiguration().getDouble("builder.spawn.y");
        double z = getFileConfiguration().getDouble("builder.spawn.z");
        float yaw = getFileConfiguration().getInt("builder.spawn.yaw");
        float pitch = getFileConfiguration().getInt("builder.spawn.pitch");

        Location loc = null;
        loc.setWorld(Bukkit.getWorld(world));
        loc.setX(x);
        loc.setY(y);
        loc.setZ(z);
        loc.setYaw(yaw);
        loc.setPitch(pitch);

        return loc;
    }

    public static Location getSurvivalSpawn() {
        String world = getFileConfiguration().getString("survival.spawn.world");
        double x = getFileConfiguration().getDouble("survival.spawn.x");
        double y = getFileConfiguration().getDouble("survival.spawn.y");
        double z = getFileConfiguration().getDouble("survival.spawn.z");
        float yaw = getFileConfiguration().getInt("survival.spawn.yaw");
        float pitch = getFileConfiguration().getInt("survival.spawn.pitch");

        Location loc = null;
        loc.setWorld(Bukkit.getWorld(world));
        loc.setX(x);
        loc.setY(y);
        loc.setZ(z);
        loc.setYaw(yaw);
        loc.setPitch(pitch);

        return loc;
    }
}
