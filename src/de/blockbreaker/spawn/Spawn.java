package de.blockbreaker.spawn;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Lukas on 25.05.2015.
 */
public class Spawn extends JavaPlugin implements Listener{

    @Override
    public void onEnable() {
        FileSpawn.create();
    }

    @Override
    public void onDisable() {

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();

        if(p.hasPermission("server.builder")) {
            p.teleport(FileSpawn.getBuilderSpawn());
        } else {
            p.teleport(FileSpawn.getSurvivalSpawn());
        }
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("setgroupspawnbb")) {
            if(sender.hasPermission("server.group.setspawn") || sender.isOp()) {
                if(args.length == 0) {
                    sender.sendMessage("Gebe eine Gruppe an!");
                    return true;
                }

                if(args.length == 1) {
                    Player p = (Player) sender;
                    if(args[0].equalsIgnoreCase("builder")) {
                        FileSpawn.setBuilderSpawn(p.getWorld().getName(), p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ(), p.getLocation().getYaw(), p.getLocation().getPitch());
                        p.sendMessage("Builderspawn gesetzt!");
                        return true;
                    }

                    if(args[0].equalsIgnoreCase("survival")) {
                        FileSpawn.setSurvivalSpawn(p.getWorld().getName(), p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ(), p.getLocation().getYaw(), p.getLocation().getPitch());
                        p.sendMessage("Survivalspawn gesetzt!");
                        return true;
                    }
                    return true;
                }
            }
        }

        return false;
    }
}
